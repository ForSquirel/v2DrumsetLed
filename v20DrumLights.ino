// Original submission 11.03.2015. 
// See https://www.youtube.com/watch?v=_kTH6uWQhTk for sample demo

#include <LiquidCrystal.h>
LiquidCrystal lcd(12, 7, 5, 4, 3, 2);
int led1 = 6;  // the pin that the LED is attached to
int led2 = 9;  // the pin that the LED is attached to
int led3 = 10; // the pin that the LED is attached to
int led4 = 11; // the pin that the LED is attached to
int brightness = 0; // how bright the LED is
int fadeAmount = 5;  // how many points to fade the LED by
int x = 0; // its a counter variable
int rate = A2; // speed/bpm/tempo value pin
int what = 0;  // 'random' value for random LED section
int led = 0;  // LED variable during random LED section
int buttonPin = 8;  // the pin that the pushbutton is attached to
int buttonPushCounter = -1; // counter for the number of button presses
int buttonState = 0;  // current state of the button
int lastButtonState = 0;  // previous state of the button
int rocker = 13; // dis(en)able sequencer
char* statusLed[] = {"Chaser", "Metronome", "Fader", "Chaser 2", "Metronome 2", "Metronome 3", "Knight Rider", "Wig-Wag", "Alternating", "In-Out-Wig-Wag", "Cascade Fader", "Fast Chase", "Cascade Chase", "We Will Rock You"}; //if you add here you must increment button push counter max in code below
int knob = A4; 
int jobStatus = 1;
char* sayings[] = {"Saying one", "Saying two", "Saying three", "Saying four", "Saying five", "Saying six", "Saying seven", "Saying eight", "Saying nine","Saying ten", "Saying eleven", "Saying twelve", "Saying 13", "Saying 14"}; //if added sayings increment random to reflect the same
void setup() {
    lcd.begin(16, 2);  // Must have for setCursor to work. I'm so dumb :(
    pinMode(led1, OUTPUT);
    pinMode(led2, OUTPUT);
    pinMode(led3, OUTPUT);
    pinMode(led4, OUTPUT);
    Serial.begin(9600);
    lcd.clear();
    lcd.home();
    lcd.print("Drum Lights 2.0");
    randomSeed(analogRead(knob)); // just some random interference
    what = random(0, 13);  //increment here if startup saying is increased or decrement if decreased
    lcd.setCursor(0, 1);
    lcd.print(sayings[what]);
    pinMode(rocker, INPUT);
    pinMode(buttonPin, INPUT);
    delay(5000);
}
//write LED brightness and cut down on code by calling function
void ledBrightness(int howBright){
    analogWrite(led1, howBright);
    analogWrite(led2, howBright);
    analogWrite(led3, howBright);
    analogWrite(led4, howBright);
}
//write LED low and cut down on code by calling function
void ledLow(){
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
    digitalWrite(led4, LOW);
}
//write LED High and cut down on code by calling function
void ledHigh(){
    digitalWrite(led1, HIGH);
    digitalWrite(led2, HIGH);
    digitalWrite(led3, HIGH);
    digitalWrite(led4, HIGH);
}
//udapte LCD display with 'current' speed
void updateDisplay(int pMin, int pMax, String effect){
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(effect);
    lcd.setCursor(0, 1);
    lcd.print("Speed:");
    lcd.setCursor(7, 1);
    lcd.print(map(analogRead(rate), 1023, 0, pMin, pMax));
}
            
void loop() {
    //Serial.println(digitalRead(rocker));
    //execute this section if toggle is off, used for selecting pattern variable
    //via button presses
    while(digitalRead(rocker) == LOW){
        if(jobStatus == 1){
            lcd.clear();
            lcd.print("Set Effect Now");
            Serial.println("Setting job status to 0");
            jobStatus = 0;
            ledLow();
        }
        Serial.println(digitalRead(rocker));
        //Serial.println(buttonPushCounter);
        lcd.home();
        //lcd.print("Ready");
        Serial.println(statusLed[buttonPushCounter]);
        lastButtonState = buttonState;
        buttonState = digitalRead(buttonPin);
        if (buttonState != lastButtonState) {
            if (buttonState == HIGH) {
                buttonPushCounter++;
                if(buttonPushCounter >= 14){  //increment this if adding effects or decrement if taking away
                    buttonPushCounter = 0;
                }
                lcd.clear();
                lcd.print(statusLed[buttonPushCounter]);
                lcd.setCursor(0, 2);
                lcd.print("Setting effect");
            }
        }
        delay(250);
    }
    //execute this section if toggle is on by running selected choice
    while(digitalRead(rocker) == HIGH){
        jobStatus = 1;
        Serial.println("Setting Job status to 1");
        //Serial.println(digitalRead(rocker));
        //Lights chase from LED1 -> LED4 and back from LED4 -> LED1
        if(buttonPushCounter == 0){
            //Serial.println("Chaser");
            lcd.setCursor(0, 1);
            lcd.clear();
            lcd.print("Chaser");
            updateDisplay(0, 99, "Chaser");
            x = 0;
            while(x < 4){
                if(x == 0) led = led1;
                updateDisplay(0, 99, "Chaser");
                if(x == 1) led = led2;
                updateDisplay(0, 99, "Chaser");;
                if(x == 2) led = led3;
                updateDisplay(0, 99, "Chaser");
                if(x == 3) led = led4;
                updateDisplay(0, 99, "Chaser");;
                while(brightness <= 255){
                    analogWrite(led, brightness);
                    brightness = brightness + fadeAmount;
                    delay(map(analogRead(rate), 0, 1023, 10, 120));
                }
                brightness = 0;
                x++;
            }
            x = 4;
            while(x > 0){
                if(x == 1) led = led1;
                updateDisplay(0, 99, "Chaser");
                if(x == 2) led = led2;
                updateDisplay(0, 99, "Chaser");
                if(x == 3) led = led3;
                updateDisplay(0, 99, "Chaser");
                if(x == 4) led = led4;
                updateDisplay(0, 99, "Chaser");
                brightness = 255;
                while(brightness >= 0){
                    analogWrite(led, brightness);
                    brightness = brightness - fadeAmount;
                    delay(map(analogRead(rate), 0, 1023, 10, 120));
                }
                x--;
            }
            fadeAmount = 5;
            brightness = 0;
            ledBrightness(brightness);
        }//end chaser
        //Simple 'metronome' to count beats (60-180BPM) beats are written HIGH
        if(buttonPushCounter == 1){
            //Serial.println("BPM");
            //lcd.setCursor(0, 1);
            //lcd.print("Metronome");
            updateDisplay(60, 180, "Metronome");
            x = 0;
            while(x < 4){
                ledHigh();
                delay(map(analogRead(rate), 0, 1023, 300, 950));
                ledLow();
                delay(map(analogRead(rate), 0, 1023, 33, 50));
                x++;
            }
        }//end metronome 1
        //all 4 lights fade in and out at the same time
        if(buttonPushCounter == 2){
            //Serial.println("FADER");
            lcd.home();
            lcd.print("Fader");
            updateDisplay(0, 100, "Fader");
            ledBrightness(brightness);
            brightness = brightness + fadeAmount;
            if (brightness == 0 || brightness == 255) {
                fadeAmount = -fadeAmount ;
                delay(map(analogRead(rate), 0, 1023, 120, 500));
            }
        }//end fader
        //Light chases from LED1 -> LED4 and cycles back to LED1 (circular)
        if(buttonPushCounter == 3){
            //Serial.println("Chase 2");
            //lcd.setCursor(0, 1);
            //lcd.print("Chaser #2");
            updateDisplay(0, 99, "Chaser #2");
            ledBrightness(brightness);
            x = 0;
            while(x < 4){
                if(x == 0) led = led1;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 1) led = led2;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 2) led = led3;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 3) led = led4;
                updateDisplay(0, 99, "Chaser #2");
                brightness = 0;
                while(brightness <= 255){
                    analogWrite(led, brightness);
                    brightness = brightness + fadeAmount;
                    delay(map(analogRead(rate), 0, 1023, 10, 120));
                }
                x++;
            }
            ledBrightness(brightness);
            x = 0;
            while(x < 4){
                if(x == 0) led = led1;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 1) led = led2;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 2) led = led3;
                updateDisplay(0, 99, "Chaser #2");
                if(x == 3) led = led4;
                updateDisplay(0, 99, "Chaser #2");
                brightness = 0;
                while(brightness <= 255){
                    analogWrite(led, brightness);
                    brightness = brightness + fadeAmount;
                    delay(map(analogRead(rate), 0, 1023, 10, 120));
                }
                x++;
            }
        }//end chaser 2
        //all lights stay low and the 'beat' is made HIGH
        if(buttonPushCounter == 4){
            //Serial.println("BPM 2");
            //lcd.setCursor(0, 2);
            //lcd.print("Metronome #2");
            updateDisplay(60, 180, "Metronome #2");
            ledLow();
            x = 0;
            what = 0;
            led = 0;
            while(x < 4){
                what = random(1,5);
                if(what == 1) led = led1;
                //updateDisplay(60, 180, "Metronome #2");
                if(what == 2) led = led2;
                //updateDisplay(60, 180, "Metronome #2");
                if(what == 3) led = led3;
                //updateDisplay(60, 180, "Metronome #2");
                if(what == 4) led = led4;
                //updateDisplay(60, 180, "Metronome #2");
                digitalWrite(led, HIGH);
                delay(map(analogRead(rate), 0, 1023, 300, 950));
                digitalWrite(led, LOW);
                delay(map(analogRead(rate), 0, 1023, 33, 50));
                x++;
                ledLow();
            }
        }//end metronome 2
        //all lights stay on and the 'beat' is made LOW
        if(buttonPushCounter == 5){
            Serial.println("BPM3");
            //lcd.setCursor(0, 2);
            //lcd.clear();
            //lcd.print("Metronome 3");
            updateDisplay(60, 180, "Metronome #3");
            ledHigh();
            x = 0;
            what = 0;
            led = 0;
            while(x < 4){
                what = random(1,5);
                if(what == 1) led = led1;
                // updateDisplay(60, 180, "Metronome #3");
                if(what == 2) led = led2;
                // updateDisplay(60, 180, "Metronome #3");
                if(what == 3) led = led3;
                //updateDisplay(60, 180, "Metronome #3");
                if(what == 4) led = led4;
                //updateDisplay(60, 180, "Metronome #3");
                digitalWrite(led, HIGH);
                delay(map(analogRead(rate), 0, 1023, 300, 950));
                digitalWrite(led, LOW);
                delay(map(analogRead(rate), 0, 1023, 33, 50));
                ledHigh();
                x++;
            }
        }//end metronome 3
        if(buttonPushCounter == 6){
          fadeAmount = 10;
            //Serial.println("Chaser");
            lcd.setCursor(0, 1);
            lcd.clear();
            lcd.print("Larson");
            updateDisplay(0, 99, "Larson");
            x = 0;
            while(x < 4){
                if(x == 0) led = led1;
                updateDisplay(0, 99, "Larson");
                if(x == 1) led = led2;
                updateDisplay(0, 99, "Larson");;
                if(x == 2) led = led3;
                updateDisplay(0, 99, "Larson");
                if(x == 3) led = led4;
                updateDisplay(0, 99, "Larson");;
                while(brightness <= 255){
                    analogWrite(led, brightness);
                    brightness = brightness + fadeAmount;
                    delay(5);
                }
                brightness = 0;
                x++;
            }
            x = 3;
            while(x > 0){
                if(x == 3) led = led4;
                updateDisplay(0, 99, "Larson");
                if(x == 2) led = led3;
                updateDisplay(0, 99, "Larson");
                if(x == 1) led = led2;
                updateDisplay(0, 99, "Larson");
                if(x == 0) led = led1;
                updateDisplay(0, 99, "Larson");
                brightness = 255;
                while(brightness >= 0){
                    analogWrite(led, brightness);
                    brightness = brightness - fadeAmount;
                    delay(5);
                }
                x--;
            }
            fadeAmount = 10;
            brightness = 0;
            ledBrightness(brightness);
            delay(200);
        }
        if(buttonPushCounter == 7){
            //Serial.println("Wig-Wag");
            //lcd.setCursor(0, 1);
            //lcd.print("Wig-Wag");
            updateDisplay(1, 10, "Wig-Wag");
            //x = 0;
            //while(x < 4){
              digitalWrite(led1, HIGH);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, LOW);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, HIGH);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, LOW);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, LOW);
              digitalWrite(led2, LOW);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, LOW);
              digitalWrite(led2, LOW);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
            //}
        }//end wigwag 1
        if(buttonPushCounter == 8){
            //Serial.println("Alternating");
            //lcd.setCursor(0, 1);
            //lcd.print("Alternating");
            updateDisplay(1, 10, "Alternating");
            //x = 0;
            //while(x < 4){
              digitalWrite(led1, HIGH);
              digitalWrite(led2, LOW);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, HIGH);
              digitalWrite(led2, LOW);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, LOW);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, LOW);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 50, 100));
            //}
        }//end Alternating
        if(buttonPushCounter == 9){
            //Serial.println("In-Out-Wig-Wag");
            //lcd.setCursor(0, 1);
            //lcd.print("In-Out-Wig-Wag");
            updateDisplay(1, 10, "In-Out-Wig-Wag");
            //x = 0;
            //while(x < 4){
              digitalWrite(led1, HIGH);
              digitalWrite(led2, LOW);
              digitalWrite(led3, LOW);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 100, 250));
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 100, 250));
              digitalWrite(led1, HIGH);
              digitalWrite(led2, LOW);
              digitalWrite(led3, LOW);
              digitalWrite(led4, HIGH);
              delay(map(analogRead(rate), 0, 1023, 100, 250));
              digitalWrite(led1, LOW);
              digitalWrite(led2, HIGH);
              digitalWrite(led3, HIGH);
              digitalWrite(led4, LOW);
              delay(map(analogRead(rate), 0, 1023, 100, 250));
            //}
        }//end In-Out-Wig-Wag
        //all 4 lights fade in and out at the same time
        if(buttonPushCounter == 10){
            //Serial.println("Cascade");
            lcd.home();
            lcd.print("Cascade");
            updateDisplay(0, 99, "Cascade");
                while(brightness <= 255){
                    analogWrite(led1, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led2, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led3, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led4, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    brightness = brightness + 25;
                    //delay(map(analogRead(rate), 0, 1023, 125, 250));
                    updateDisplay(0, 99, "Cascade Fader");;
                }
                brightness = 0;
        }//end Cascade
        if(buttonPushCounter == 11){
            //Serial.println("Fast Chase");
            lcd.home();
            lcd.print("Fast Chase");
            updateDisplay(0, 99, "Fast Chase");
                while(brightness <= 75){
                    analogWrite(led1, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led2, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led3, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led4, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    brightness = brightness + 25;
                    //delay(map(analogRead(rate), 0, 1023, 125, 250));
                    updateDisplay(0, 99, "Fast Chase");;
                }
                brightness = 0;
        }//end fast chase
        if(buttonPushCounter == 12){
            //Serial.println("Cascade Chase");
            lcd.home();
            lcd.print("Cascade Chase");
            updateDisplay(0, 99, "Cascade Chase");
                while(brightness <= 75){
                    analogWrite(led1, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led2, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led3, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led4, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    brightness = brightness + 25;
                    //delay(map(analogRead(rate), 0, 1023, 125, 250));
                    updateDisplay(0, 99, "Cascade Chase");;
                }
                while(brightness > 0){
                    analogWrite(led1, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led2, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led3, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    analogWrite(led4, brightness);
                    delay(map(analogRead(rate), 0, 1023, 50, 100));
                    brightness = brightness - 25;
                    //delay(map(analogRead(rate), 0, 1023, 125, 250));
                    updateDisplay(0, 99, "Cascade Chase");;
                }
                brightness = 0;
        }//end cascade chase
        //Attempted to push out beats along with Queen's "We Will Rock You"
        if(buttonPushCounter == 13){
            //Serial.println("Queen");
            //lcd.setCursor(0, 1);
            //lcd.print("Queen");
            brightness = 200;
            ledLow();
            updateDisplay(11, 11, "We Will Rock You");
            delay(30);
            analogWrite(led1, brightness);
            delay(350);
            analogWrite(led4, brightness);
            delay(350);
            analogWrite(led2, brightness);
            analogWrite(led3, brightness);
            delay(700);
        }//end Queen
        fadeAmount = 5;
    }
}
