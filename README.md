# v2DrumLight
Version 2 of my drum set LED lights project

Most of the changes have been in hardware but a lot of software changes were made as well. A new LCD screen replaces the LED's allowing for more effects and a readable display instead of the original single LED for each effect. There is nothing to complicated about the project but it is fun when implemented. 

Software uses 4 controls
  1) Power On/Off
  2) Effects Active/Inactve
  3) Rate/Speed/Tempo adjustment
  4) Effects Selector
  
*add more later
